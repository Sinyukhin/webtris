const RESPONSE_DELAY = 50;
const SPACE = 32;
const LEFT = 37;
const RIGHT = 39;
const DOWN = 40;

const COUNT_X = 10;
const COUNT_Y = 20;
const BLOCK_SIZE = 20;

/* Calculating collision front of box and game field */
function getFront(field, box) {
	var stripe;
	var front = [];
	for (stripe=box.left; stripe<=box.right; stripe++)
		for (var k=box.top; k>=0; k--)
			if (field.data[stripe][k] > 0)
				front.push({i:stripe, j:k});
	return front;
}

/* Fix figure onto game field */
function harden (figure, field) {
	var k;
	for (k=0; k<figure.blocks.length; k++) {
		field.data[figure.blocks[k].i][figure.blocks[k].j] = 1;
	}
}

/* Key processing */
var Key = {
	pressed : {},
	
	isDown: function(keyCode) {
		return this.pressed[keyCode];
	},
	onKeydown: function(event) {
		var key = event.which || event.keyCode;
		this.pressed[key] = true;
	},
	onKeyup: function(event) {
		var key = event.which || event.keyCode;
		delete this.pressed[key];
	}
};

/* Game Engine */
function Game() {
	this.field = new GameField(COUNT_X, COUNT_Y);
	this.figure = new Figure(Math.floor(COUNT_X/2), COUNT_Y-1, 0);
	this.renderer = new Renderer();
	this.scores = 0;
	this.speed = 10;
}

Game.prototype.start = function () {
	this.renderer.redrawFigure(this.figure);
	
	window.addEventListener('keyup', 
		function(event) { 
			Key.onKeyup(event); 
		}, false);
	
	window.addEventListener('keydown', 
		function(event) { Key.onKeydown(event); 
		}, false);
	
	var keyActions = {};
	keyActions[LEFT] = function() {
		if (this.figure.move(this.field, -1)) {
			this.figure.updateFront(this.field);
			this.renderer.redrawFigure(this.figure);
		}
	}.bind(this);
	keyActions[RIGHT] = function () {
		if (this.figure.move(this.field, 1)) {
			this.figure.updateFront(this.field);
			this.renderer.redrawFigure(this.figure);
		}
	}.bind(this);
	keyActions[SPACE] = function () {
		this.figure.checkAndRotate(this.field);
		this.renderer.redrawFigure(this.figure);
	}.bind(this);
	
	/* Game loop */
	var ticks = 0;
	var speedup = false;
	var id = setInterval (function() {
		
		/* 1. Handle keyboard keys at each tick of timer */
		if (Key.isDown(LEFT)) keyActions[LEFT]();
		if (Key.isDown(RIGHT)) keyActions[RIGHT]();
		if (Key.isDown(SPACE)) keyActions[SPACE]();
		/* Speedup of falling, temporary solition */
		if (Key.isDown(DOWN)) {
			if (!speedup) {
				this.backup_speed = this.speed;
				this.speed = 1;
			}
			speedup = true;
			console.log('saving backup speed', this.backup_speed);
		}
		
		else if (this.backup_speed > 0) {
			console.log('restore from backup speed', this.backup_speed);
			this.speed = this.backup_speed;
			this.backup_speed = 0;
			speedup = false;
		}
		
		/* 2. Lower object once at this.speed ticks */
		if (ticks >= this.speed) {
			ticks = 0;
			if (this.figure.canFallDown()) {
				this.figure.lower();
				this.renderer.redrawFigure(this.figure);
			}
			else {
				harden(this.figure, this.field);
				console.log(this.field.data);
			
				var lines = this.field.getLinesToRemove();
				/* remove lines and redraw only if necessary */
				if (lines.length > 0) {
					console.log('to_remove', lines);
					this.field.removeLines(lines);
					this.renderer.redrawField(this.field);
				}
				/* create new figure at respawning place */
				this.figure = new Figure(getRandomInt(1, COUNT_X-2), COUNT_Y-1, 
					getRandomInt(0, figureMasks.length-1));
				// check if figure can't be placed
				if (!figureCanSpawn(this.figure, this.field)) {
					clearInterval(id);
					this.renderer.printGameOver();
					return;
				}
				this.figure.updateFront(this.field);
				this.renderer.redrawFigure(this.figure);
			}
		}
		ticks ++;
	}.bind(this), RESPONSE_DELAY);
}

function figureCanSpawn(figure, field) {
	for (var block of figure.blocks) {
		if (field.data[block.i][block.j] > 0) //not empty
			return false;
	}
	return true;
}
