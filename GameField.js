
function GameField(countX, countY) {
	this.blockCountY = countY;
	this.blockCountX = countX;
	this.data = [];
	this.init();
}

/* Init array of blocks */
GameField.prototype.init = function() {
	for (var i=0; i<this.blockCountX; i++) {
		this.data[i] = [];
		for (var j=0; j<this.blockCountY; j++)
			this.data[i][j] = 0;
	}
}

/* Clear lines specified in list 'lines' with shifting by single array pass */
GameField.prototype.removeLines = function (lines) {
	var k,p,i,_shift = 1;
	var till;
	lines.reverse();
	for (k=0; k<lines.length; k++) {
		till = (k+1 < lines.length)?lines[k+1]:this.blockCountY;
		for (p=lines[k]+1; p<till; p++) {
			for (i=0; i<this.blockCountX; i++)
				this.data[i][p - _shift] = this.data[i][p];
		}
		_shift ++;
	}
	/* fill the top of field stack (after shifting) */
	for (p=this.blockCountY-lines.length; p<this.blockCountY; p++)
		for (i=0; i<this.blockCountX; i++)
			this.data[i][p] = 0;
}

/* Calculate number of lines to remove */
GameField.prototype.getLinesToRemove = function () {
	var k,p, to_remove, remove_fl;
	to_remove = [];
	for (p=this.blockCountY-1; p>=0; p--) {
		be_removed = true;
		for (k=0; k<this.blockCountX; k++) {
			if (this.data[k][p] <= 0) {
				be_removed = false;
				break;
			}
		}
		if (be_removed)
			to_remove.push(p);
	}
	return to_remove;
}
