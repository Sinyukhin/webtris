/* Generate random within given interval */
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/* Check whether the block lies within the box or not */
function into(block, box) {
	return (block.i >= box.left) && (block.i <= box.right) &&
		(block.j >= box.bottom) && (block.j <= box.top);
}
