/* HTML5 canvas renderer */

function Renderer() {
	this.blockSize = BLOCK_SIZE;
	this.canvas = document.createElement('canvas');
	this.canvas.height = COUNT_Y * this.blockSize;
	this.canvas.width = COUNT_X * this.blockSize;
	document.body.appendChild(this.canvas);
	this.context = this.canvas.getContext("2d");
	this.context.fillStyle = "#efefef";
	this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
}

Renderer.prototype.drawBlock = function (rect) {
	this.context.fillRect(rect.left, rect.top, this.blockSize, this.blockSize);
}

Renderer.prototype.redrawField = function (field) {
	var i,j, left, top;
	
	this.context.fillStyle = "#efefef";
	this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
	
	this.context.fillStyle = "#7777ef";
	for (i=0; i<field.blockCountX; i++) {
		for (j=0; j<field.blockCountY; j++) {
			if (field.data[i][j] > 0)
				this.drawBlock({
					top : this.canvas.height - (j+1) * this.blockSize,
					left : i * this.blockSize,
				});
		}
	}
}

Renderer.prototype.redrawFigure = function (figure) {
	var x,y, k;
	if (figure.behind) {
		for (k=0; k<figure.behind.length; k++)
			this.context.putImageData(figure.behind[k].img, 
			  figure.behind[k].left, 
			  figure.behind[k].top);
	}
	figure.behind = [];
	this.context.fillStyle = "#7777ef";
	for (k=0; k<figure.blocks.length; k++) {
		x = figure.blocks[k].i * this.blockSize;
		y = this.canvas.height - (figure.blocks[k].j + 1) * this.blockSize;
		figure.behind.push ({
			left: x,
			top: y,
			img: this.context.getImageData(x, y, this.blockSize, this.blockSize)
		});
		this.drawBlock({ left: x, top: y });
	}
}

Renderer.prototype.printGameOver = function() {
	var savedStyle = this.context.fillStyle;
	this.context.fillStyle = "#ee5555";
	this.context.font = "30px Verdana";
	this.context.textAlign = "center";
	this.context.fillText("Game Over", this.canvas.width/2, this.canvas.height/2);
	this.context.fillStyle = savedStyle;
}
