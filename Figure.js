const FigureType = {
	TYPE_S : 0,
	TYPE_L : 1,
	TYPE_I : 2,
	TYPE_O : 3,
	TYPE_T : 4,
	TYPE_J : 5,
	TYPE_Z : 6
};
/* Figure masks. The first point of mask is always pivot point */
const figureMasks = [
	[{di: 0,dj: 0}, {di: 1,dj: 0}, {di:-1,dj:-1},{di: 0,dj:-1}], // s
	[{di: 0,dj: 0}, {di: 0,dj: 1}, {di:0,dj:-1},{di: 1,dj:-1}], // L
	[{di: 0,dj: 0}, {di: 0,dj: 1}, {di:0,dj:-1},{di: 0,dj:-2}], // I
	[{di: 0,dj: 0}, {di: 0,dj: 1}, {di:-1,dj:1},{di: -1,dj:0}], // O
	[{di: 0,dj: 0}, {di: 1,dj: 0}, {di:-1,dj: 0},{di: 0,dj: 1}], // T
	[{di: 0,dj: 0}, {di: 0,dj: 1}, {di:0,dj:-1},{di: -1,dj:-1}], // J
	[{di: 0,dj: 0}, {di:-1,dj: 0}, {di:1,dj:-1},{di: 0,dj:-1}] // z
];

const figurePivots = [
	{di: 0,dj: 0}, {di: 0,dj: 0}, {di: 0.5,dj: -0.5}, {di: -0.5,dj: 0.5}, {di: 0,dj: 0}, {di: 0,dj: 0}, {di: 0,dj: 0}
];

const ROT_LEFT = [ [0, 1], [-1, 0] ];
const ROT_RIGHT = [ [0, -1], [1, 0] ];

function getBoundBox(blocks) {
	var box={
		left: Infinity, 
		right: 0, 
		top: 0, 
		bottom: Infinity
	};
	/* TODO: possibly rewrite with reduce */
	for (var k=0; k<blocks.length; k++) {
		if (blocks[k].i < box.left)
			box.left = blocks[k].i;
		if (blocks[k].i > box.right)
			box.right = blocks[k].i;
		if (blocks[k].j < box.bottom)
			box.bottom = blocks[k].j;
		if (blocks[k].j > box.top)
			box.top = blocks[k].j;
	}
	return box;
}

function Figure(si, sj, type) {
	this.blocks = [];
	this.front = [];
	this.type = type;
	
	figureMasks[type].forEach(function (item, i, arr) {
		this.blocks.push({i: si + item.di, j: sj + item.dj});
	}.bind(this)); // using bind to pass this
	this.pivot = {
		i: si + figurePivots[type].di,
		j: sj + figurePivots[type].dj
	};
	
	this.canFallDown = function()  {
		var k,p, dist;
		var box = getBoundBox(this.blocks);
		box.top--;
		box.bottom--;
		for (p=0; p<this.front.length; p++) {
			/* It's time to check possible coollision if block is inside a figure */
			if (into(this.front[p], box)) {
				/* Check all figure blocks for possible collision with front box */
				for (k=0; k<this.blocks.length; k++) {
					dist = Math.abs(this.blocks[k].i - this.front[p].i) +
							Math.abs(this.blocks[k].j-1 - this.front[p].j);
					if (dist < 1)
						return false;
				}
			}
		}
		return (box.bottom >= 0);
	};
	
	this.updateFront = function (field) {
		var box = getBoundBox(this.blocks);
		this.front = getFront(field, box);
	};
	
	/* drop the figure on the Y axis */
	this.lower = function () {
		for (var k=0; k<this.blocks.length; k++) {
			this.blocks[k].j--;
		}
		this.pivot.j--;
	};
	
	this.move = function (field, d_i) {
		var box = getBoundBox(this.blocks);
		box.left += d_i;
		box.right += d_i;
		if (box.left < 0 || box.right >= field.blockCountX)
			return false; // unable to move
		/* Check for barrier which prevents moving */
		for (var p=0; p<this.blocks.length; p++) {
			var new_i = this.blocks[p].i + d_i;
			if (field.data[new_i][this.blocks[p].j] > 0)
				return false;
			}
		/* Move blocks */
		for (var p=0; p<this.blocks.length; p++)
			this.blocks[p].i += d_i;
		this.pivot.i += d_i;
		/* Recalculate collision front */
		this.updateFront(field);
		return true; // moved successfully
	};
	 
	this.checkAndRotate = function(field) {
		var d_i,d_j,k,new_i,new_j;
		var r_blocks = {};
		/*var pivot = {
			i : this.blocks[0].i, 
			j: this.blocks[0].j
		};*/
		var pivot = this.pivot;
		for (var k=0; k<this.blocks.length; k++) {
			d_i = (this.blocks[k].i - pivot.i) * ROT_LEFT[0][0] + 
			        (this.blocks[k].j - pivot.j) * ROT_LEFT[0][1];
			              
			d_j = (this.blocks[k].i - pivot.i) * ROT_LEFT[1][0] + 
			        (this.blocks[k].j - pivot.j) * ROT_LEFT[1][1];
			r_blocks[k] = { i: Math.round(pivot.i + d_i), j: Math.round(pivot.j + d_j)};
			
			/* Simple rotation validation */
			if (r_blocks[k].j < 0 || r_blocks[k].i < 0 || 
			    r_blocks[k].i >= field.blockCountX )
				return false; // not possible to rotate (out of game field)
			
			if (field.data[r_blocks[k].i][r_blocks[k].j] > 0)
			/* not possible to rotate (there is a barrier) */
				return false; 
		}
		/* update blocks with r_blocks if rotation is possible */
		for (var k=0; k<this.blocks.length; k++)
			this.blocks[k] = r_blocks[k];
		
		this.updateFront(field);
		return true;
	};
	
	this.dump = function () {
		console.log('current block:',this.blocks);
	}
}
